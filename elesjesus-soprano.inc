\context Staff = "soprano" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Soprano"
	\set Staff.shortInstrumentName = "S."
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-soprano" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\clef "treble"
		\key d \major

		R1*4  |
%% 5
		a 4. b d' 4  |
		e' 4. fis' a' 4  |
		g' 4 g' 8 fis' 4. d' 4  |
		b 2. e' 4  |
		d' 4. e' fis' 4  |
%% 10
		e' 4. fis' a' 4  |
		g' 4 g' 8 fis' 4 g' 8 fis' 4  |
		d' 2. r4  |
		r8 a' a' a' a' 8. a' a' 8  |
		a' 8. g' fis' 8 d' 2  |
%% 15
		r4 a' 8 a' a' a' a' a'  |
		a' 8. g' fis' 8 d' 2  |
		d' 4 d' 8 e' 4. fis' 4  |
		e' 4. fis' a' 4  |
		g' 4. fis' d' 4  |
%% 20
		b 1  |
		R1*2  |
		a 4. b d' 4  |
		e' 4. fis' a' 4  |
%% 25
		g' 4. fis' d' 4  |
		b 2. e' 4  |
		d' 4. e' fis' 4  |
		e' 4. fis' a' 4  |
		g' 4 g' 8 fis' 4. d' 4  |
%% 30
		b 2. r4  |
		r8 a' a' a' a' 8. a' a' 8  |
		a' 8. g' fis' 8 d' 4. r8  |
		r4 a' 8 a' a' 8. a' a' 8  |
		a' 8. g' fis' 8 d' 4. r8  |
%% 35
		r4 a' 8 a' a' 8. a' ~ a' 8  |
		a' 8. g' fis' 8 d' 2 (  |
		e' 2 ~ e' 8 fis' e' d' ~  |
		d' 4. ) r8 fis' 8. g' a' 8  |
		a' 2. ~ a' 16 a' 8.  |
%% 40
		a' 8. g' fis' 8 d' 4. r8  |
		r4 a' 8 a' a' 8. a' a' 8  |
		a' 8. g' fis' 8 d' 4. r8  |
		r8 a' a' a' a' 8. a' a' 8  |
		g' 4. g' 4 fis' e' 8 ~  |
%% 45
		e' 1  |
		r2 fis' 8. g' a' 8  |
		a' 2. ~ a' 16 a' 8.  |
		a' 8. g' fis' 8 fis' 4. r8  |
		r8 g' g' g' g' 8. g' g' 8  |
%% 50
		g' 8. fis' e' 8 fis' 4. r8  |
		r8 fis' fis' fis' fis' 8. fis' fis' 8  |
		g' 4. g' 4 fis' e' 8 ~  |
		e' 4. ~ e' 4 r8 r16 b 8.  |
		g' 4 g' 8 g' 4 g' fis' 8 ~  |
%% 55
		fis' 1  |
	}

	\new Lyrics \lyricsto "voz-soprano" {
		"Si es" que te sien -- tes va -- cí -- "o en" "tu in" -- te -- rior
		y "no en" -- cuen -- tras na -- da que lle -- ne tu co -- ra -- zón,
		quie -- ro de -- cir -- te "que a" -- hí él es -- tá
		es -- pe -- ran -- do lle -- nar e -- se lu -- gar,
		só -- "lo es" -- pe -- ra que tú le de -- jes en -- trar.

		Quie -- ro de -- cir -- te que su gran a -- mor,
		no hay quién "lo i" -- gua -- le, en to -- da la crea -- ción.
		Quie -- ro de -- cir -- te que él sien -- do Dios,
		"se hi" -- zo hom -- bre "y en" la cruz mu -- rió
		pa -- ra dar -- nos la sal -- va -- ción. __

		Él es Je -- sús, el Hi -- jo de Dios,
		el ca -- mi -- "no y" la re -- su -- rrec -- ción,
		quien cre -- "a en" él vi -- "da e " -- ter -- na, ten -- drá. __

		Él es Je -- sús, él es el Se -- ñor,
		él es la ro -- ca de la sal -- va -- ción,
		quien cre -- "a en" él vi -- "da e" -- ter -- na ten -- drá, __
		por siem -- pre vi -- vi -- rá. __
	}
>>
