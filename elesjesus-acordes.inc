\context ChordNames
	\chords {
		\set chordChanges = ##t
		% intro
		d1 a1 g1 g1

		% si es que te sientes...
		d1 a1 g1 g1
		d1 a1 g1 g1
		d1 g1 d1 g1
		d1 a1 g1 g1
		a1 a1

		d1 a1 g1 g1
		d1 a1 g1 g1
		d1 g1 d1 g1
		d1 g1 a1 g1

		% el es jesuuus...
		d1 g1 d1 g1
		d1 g1 a1 a1
		b1:m fis1:m g1 d1
		b1:m g1 a1 g1

		d1 %g1 d1 a1
	}
