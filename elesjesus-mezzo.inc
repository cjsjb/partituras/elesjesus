\context Staff = "mezzo" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Mezzosoprano"
	\set Staff.shortInstrumentName = "M."
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-mezzo" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\clef "treble"
		\key d \major

		R1*4  |
%% 5
		a 4. b d' 4  |
		e' 4. fis' a' 4  |
		g' 4 g' 8 fis' 4. d' 4  |
		b 2. e' 4  |
		d' 4. e' fis' 4  |
%% 10
		e' 4. fis' a' 4  |
		g' 4 g' 8 fis' 4 g' 8 fis' 4  |
		d' 2. r4  |
		r8 fis' fis' fis' fis' 8. fis' fis' 8  |
		fis' 8. e' d' 8 b 2  |
%% 15
		r4 fis' 8 fis' fis' fis' fis' fis'  |
		fis' 8. e' d' 8 b 2  |
		d' 4 d' 8 e' 4. fis' 4  |
		e' 4. fis' a' 4  |
		g' 4. fis' d' 4  |
%% 20
		b 1  |
		R1*2  |
		a 4. b d' 4  |
		e' 4. fis' a' 4  |
%% 25
		g' 4. fis' d' 4  |
		b 2. e' 4  |
		d' 4. e' fis' 4  |
		e' 4. fis' a' 4  |
		g' 4 g' 8 fis' 4. d' 4  |
%% 30
		b 2. r4  |
		r8 fis' fis' fis' fis' 8. fis' fis' 8  |
		fis' 8. e' d' 8 b 4. r8  |
		r4 fis' 8 fis' fis' 8. fis' fis' 8  |
		fis' 8. e' d' 8 b 4. r8  |
%% 35
		r4 fis' 8 fis' fis' 8. fis' ~ fis' 8  |
		fis' 8. e' d' 8 b 2 (  |
		cis' 2 ~ cis' 8 d' cis' b ~  |
		b 4. ) r8 d' 8. e' fis' 8  |
		fis' 2. ~ fis' 16 fis' 8.  |
%% 40
		fis' 8. e' d' 8 b 4. r8  |
		r4 fis' 8 fis' fis' 8. fis' fis' 8  |
		fis' 8. e' d' 8 b 4. r8  |
		r8 fis' fis' fis' fis' 8. fis' fis' 8  |
		e' 4. e' 4 d' cis' 8 ~  |
%% 45
		cis' 1  |
		r2 d' 8. e' fis' 8  |
		fis' 2. ~ fis' 16 fis' 8.  |
		e' 8. d' cis' 8 cis' 4. r8  |
		r8 d' d' d' d' 8. d' d' 8  |
%% 50
		d' 8. d' d' 8 d' 4. r8  |
		r8 d' d' d' d' 8. d' d' 8  |
		d' 4. e' 4 fis' e' 8 ~  |
		e' 4. ~ e' 4 r8 r16 b 8.  |
		d' 4 d' 8 d' 4 g' fis' 8 ~  |
%% 55
		fis' 1  |
%		\bar "|."
	}

	\new Lyrics \lyricsto "voz-mezzo" {
		"Si es" que te sien -- tes va -- cí -- "o en" "tu in" -- te -- rior
		y "no en" -- cuen -- tras na -- da que lle -- ne tu co -- ra -- zón,
		quie -- ro de -- cir -- te "que a" -- hí él es -- tá
		es -- pe -- ran -- do lle -- nar e -- se lu -- gar,
		só -- "lo es" -- pe -- ra que tú le de -- jes en -- trar.

		Quie -- ro de -- cir -- te que su gran a -- mor,
		no hay quién "lo i" -- gua -- le, en to -- da la crea -- ción.
		Quie -- ro de -- cir -- te que él sien -- do Dios,
		"se hi" -- zo hom -- bre "y en" la cruz mu -- rió
		pa -- ra dar -- nos la sal -- va -- ción. __

		Él es Je -- sús, el Hi -- jo de Dios,
		el ca -- mi -- "no y" la re -- su -- rrec -- ción,
		quien cre -- "a en" él vi -- "da e " -- ter -- na, ten -- drá. __

		Él es Je -- sús, él es el Se -- ñor,
		él es la ro -- ca de la sal -- va -- ción,
		quien cre -- "a en" él vi -- "da e" -- ter -- na ten -- drá, __
		por siem -- pre vi -- vi -- rá. __
	}
>>
